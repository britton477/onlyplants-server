<?php
$wall_paper = "only_plants.jpg";
?>

<!DOCTYPE html>
<html>
  <head>
    <title>OnlyPlants</title>
    <style type="text/css">
      /* Font styling */
      h1 { font-family: 'Raleway Light', sans-serif; }
      h2 { font-family: 'Khula Regular', sans-serif; }
      h3 { font-family: 'Khula Regular', sans-serif; }
      p { font-family: 'Khula Regular', sans-serif; }
      li { 
        font-size: 18px;
        font-family: 'Khula Regular', sans-serif;
        list-style-type: none;
        }
        
      /* Styling for background */
      body, html {
        height: 100%;
        margin: 0;
        background-image: url('<?php echo $wall_paper;?>');
      }

      /* Style the header */
      header {
        background-color: #0CBE58;
        color: #fff;
        margin: 0;
        padding: 6px;
        height: 100px;
        display: flex;
        justify-content: flex-start;
        align-items: center;        
      }
      
      /* Style for h1 header */
      header h1 {
        font-size: 70px;
        margin: 0;
        text-align: center;
        width: 100%;
      }
      
      /* Style the footer */
      footer {
        background-color: #0CBE58;
        color: #fff;
        text-align: center;
        position: absolute;
        bottom: 0;
        width: 100%;
        margin: 0;
      }
      
      /* Style the navigation menu */
      nav {
        background-color: #07A149;
        float: left;
        width: 8%;
        padding: 20px;
        min-height: 80%;
        
      }
      
      /* Style the main content */
      .main {
        margin: 0px auto;
        padding: 20px;
        float: left;
        width: 70%;
      }
      
      header img {
        margin-right: 20px;
        width: 170px;
        height: 100px;
       }
      
      /* Clear the float */
      .clearfix::after {
        content: "";
        clear: both;
        display: table;
      }
      
    </style>
  </head>
  <body>
    <!-- Header -->
    <header>
      <img src="logo-white.png" alt="logo" />
      <h1>Only Plants</h1>
    </header>
    
    <!-- Navigation menu -->
    <nav>
      <h2>Navigation</h2>
      <ul>
        <li><a href="index.php">Home</a></li>
        <li><a href="temperature.php">Past Temps</a></li>
        <li><a href="humidity.php">Humidity</a></li>
        <li><a href="moisture.php">Soil Moisture</a></li>
        <li><a href="about_us.php">About Us</a></li>
      </ul>
    </nav>

    <!-- Main content -->
    <div class="main">
      <!-- Title with border -->
      <h1>Welcome to OnlyPlants</h1>
      <h2>About us...</h2>
      <p>
        Welcome to OnlyPlants, your go-to destination for nature monitoring and plant care.<br>
        At OnlyPlants, we understand the deep connection between humans and nature, and we believe that every plant deserves the best possible care.<br>
        Our mission is to empower plant enthusiasts like you to create thriving and vibrant indoor gardens.<br>
      </p>
      <p>
        Led by founder Aaron Britton, a passionate nature lover and plant enthusiast, OnlyPlants brings together cutting-edge technology and a love for greenery<br>
        With years of experience and a deep understanding of the needs of different plant species,<br>
        Aaron embarked on a journey to develop a platform that provides accurate and real-time data on temperature, moisture, and humidity levels for house plants.<br>
      </p>
      <p>
        At OnlyPlants, we take pride in our commitment to delivering precise and reliable information. <br>
        Our state-of-the-art monitoring system ensures that you have access to the most up-to-date data about your plants. <br>
        Enabling you to make informed decisions and provide optimal conditions for their growth. <br>
        Whether you are a seasoned gardener or just starting your plant journey, we are here to support you every step of the way.<br>
      </p>
      <p>
        We value your trust and strive to provide exceptional customer service.<br>
        If you have any questions, concerns, or simply want to share your plant stories, please don't hesitate to reach out to us.<br>
        You can contact Aaron Britton directly at contact@axbritton.co.uk.<br>
        We would love to hear from you and help you cultivate your indoor oasis with OnlyPlants.<br>
      </p>
    </div>
    <!-- Clear the float -->
    <div class="clearfix"></div>

    <!-- Footer -->
    <footer>
      <p>Designed and Developed by Aaron Britton</p>
    </footer>
  </body>
</html>
