<?php

# Function to connect to the database
function OpenCon()
{
    $creds = json_decode(file_get_contents("/etc/creds.json"), true);
    $dbhost = $creds["dbhost"];
    $dbuser = $creds["username"];
    $dbpass = $creds["password"];
    $db = $creds["database"];
    $conn = new mysqli($dbhost, $dbuser, $dbpass,$db) or die("Connect failed: %s\n". $conn -> error);
    
    return $conn;
}

function CloseCon($conn)
{
    $conn -> close();
}

?>