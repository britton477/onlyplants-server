Welcome to OnlyPlants!  

A PHP lamp stack project monitoring the enviormental conditions of some plants.  
PHP is used for the backend.  
HMTL and CSS for the front end.  
The data is retrived from an Arduino.  
I used a JSON API to present the data from a MySQL database.  
I have switched to gitlabs pipelines to allow continuous deployment to the server.  

You can find it live here --> http://axbritton.co.uk/onlyplants-server  

![Semantic description of image](/doc/onlyplants\ miro.png "Project diagram")
